
 /**
 * Baijiahulian.com Inc.
 * Copyright (c) 2014-2016 All Rights Reserved.
 */
    
package cn.wenhao.javaClassReload.customizeExceptions;


/**
 *  @say little Boy, don't be sad.
 *  @name Rezar
 *  @time Oct 27, 2016
 *  @Desc this guy is too lazy, nothing left.
 */

public class UnableToLoadClassException extends RuntimeException {

    /**
     * @param slashedclassname
     */
    public UnableToLoadClassException(String errorMsg) {
        super(errorMsg);
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}

    