
 /**
 * Baijiahulian.com Inc.
 * Copyright (c) 2014-2016 All Rights Reserved.
 */
    
package cn.wenhao.javaClassReload.customizeExceptions;

import java.io.IOException;

/**
 *  @say little Boy, don't be sad.
 *  @name Rezar
 *  @time Oct 27, 2016
 *  @Desc this guy is too lazy, nothing left.
 */

public class ReloadException extends RuntimeException {

    /**
     * @param errorMsg
     * @param e
     */
    public ReloadException(String errorMsg, IOException e) {
        super(errorMsg,e);
    }

    /**
     * 
     */
        
    private static final long serialVersionUID = 1L;

}

    