
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import lombok.extern.slf4j.Slf4j;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Sep 11, 2016
 * @Desc this guy is too lazy, nothing left.
 */
@Slf4j
public class FileUtil {

    public static byte[] readFromFile(URL url) {
        log.debug("readFile:{}", url);
        byte[] emptyRet = new byte[0];
        if (url == null) {
            return emptyRet;
        }
        InputStream fis = null;
        BufferedInputStream bis = null;
        try {
            fis = url.openStream();
            bis = new BufferedInputStream(fis);
            emptyRet = new byte[fis.available()];
            bis.read(emptyRet);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                bis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return emptyRet;
    }

}
