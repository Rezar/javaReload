package cn.wenhao.javaClassReload.utils;

/**
 * @author Rezar
 * @createDate :Dec 24, 2015 8:53:52 PM
 * @desc :
 */
public class TupleUtil {

    public static final Integer OK = 200;
    public static final Integer NO_OK = 400;

    public static <A, B> TwoTuple<A, B> tuple(A a, B b) {
        return new TwoTuple<A, B>(a, b);
    }

}
