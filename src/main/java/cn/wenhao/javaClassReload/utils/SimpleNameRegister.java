
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import lombok.extern.slf4j.Slf4j;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 28, 2016
 * @Desc this guy is too lazy, nothing left.
 */
@Slf4j
public class SimpleNameRegister implements NameRegister {

    private static Map<Object, Integer> registerMaps = new HashMap<>();

    private static AtomicInteger atomicInteger = new AtomicInteger(0);

    @Override
    public Integer register(Object registerObj) {
        Integer registerNum = atomicInteger.incrementAndGet();
        registerMaps.put(registerObj, registerNum);
        log.debug("obj{} ----> {} ", registerNum);
        return registerNum;
    }

}
