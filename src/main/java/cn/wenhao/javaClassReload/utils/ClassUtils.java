
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.utils;

import java.io.InputStream;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 27, 2016
 * @Desc this guy is too lazy, nothing left.
 */
public class ClassUtils {

    public static InputStream getResourceInputStream(Class<?> clazz) {
        return clazz.getClassLoader().getResourceAsStream(clazz.getName().replace(".", "/") + ".class");
    }

}
