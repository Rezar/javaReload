package cn.wenhao.javaClassReload.zTestCode.afterChange;

import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;

import cn.wenhao.javaClassReload.JavaClassModify.interfaces.ExecutorAccessor;
import cn.wenhao.javaClassReload.constant.ObjExecutorCache;
import cn.wenhao.javaClassReload.constant.ObjFieldMemberCache;
import cn.wenhao.javaClassReload.javaClassElements.ObjExecutor;
import cn.wenhao.javaClassReload.javaClassElements.ObjFieldMemberExecutor;
import cn.wenhao.javaClassReload.utils.GenericsUtils;
import cn.wenhao.javaClassReload.zTestCode.App2;

public class App extends App2 implements ExecutorAccessor {

    @Required
    private String name;

    @Required
    private App2 app2;
    private int age;
    public ObjExecutor _obj_method_executor_$_Rezar_;
    public ObjFieldMemberExecutor _obj_field_member_$_Rezar_;

    public void setName(String name) {
        if (GenericsUtils.isNullOrEmpty(name)) {
            return;
        }
        App2.age = 12;

        System.out.println("app2's age is : " + App2.age);
        App2 app2 = new App2();
        app2.setName(name);
        boolean isOk = checkName(name, 23, "abdfd");
        if (isOk) {
            this.name = name;
            if (this.age == 123) {
                return;
            }
            name = this.name;
            test(12, getTempName(getName()), new Object[] { Integer.valueOf(23), name });
        } else {
            throw new IllegalArgumentException("name is illegal");
        }
    }

    private static void test(int age2, Object tempName, Object[] params) {
        System.out.println("out:" + age2 + tempName + params);
    }

    public Object getTempName(String tempName) {
        return tempName;
    }

    public String getName() {
        return "haha";
    }

    public boolean checkName(String name, int temp, String temp2) {
        return true;
    }

    public String toString() {
        setName("i am test ");
        return "i am test ok";
    }

    public ObjFieldMemberExecutor getObjFieldMemberExecutor() {
        return this._obj_field_member_$_Rezar_;
    }

    public ObjExecutor getObjMethodExecutor() {
        return this._obj_method_executor_$_Rezar_;
    }

    public App(String name) {
        this._obj_method_executor_$_Rezar_ = ObjExecutorCache.getObjExecutor("cn/wenhao/javaClassReload/zTestCode/App");
        this._obj_field_member_$_Rezar_ =
            ObjFieldMemberCache.getObjFieldMember("cn/wenhao/javaClassReload/zTestCode/App");

        Integer localInteger = Integer.valueOf(23);
        App localApp = this;
        ObjFieldMemberCache.putField(localApp, localInteger, "age");
    }

    public App() {
        this._obj_method_executor_$_Rezar_ = ObjExecutorCache.getObjExecutor("cn/wenhao/javaClassReload/zTestCode/App");
        this._obj_field_member_$_Rezar_ =
            ObjFieldMemberCache.getObjFieldMember("cn/wenhao/javaClassReload/zTestCode/App");

        Integer localInteger = Integer.valueOf(23);
        App localApp = this;
        ObjFieldMemberCache.putField(localApp, localInteger, "age");
    }
}