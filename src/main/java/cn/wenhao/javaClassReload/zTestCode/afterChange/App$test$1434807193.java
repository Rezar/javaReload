package cn.wenhao.javaClassReload.zTestCode.afterChange;

import cn.wenhao.javaClassReload.invoker.MethodInvoker;

public class App$test$1434807193 implements MethodInvoker {
    public App host;

    public Object invoke(Object paramObject, Object...paramArrayOfObject) {
        Integer localInteger = (Integer) paramArrayOfObject[0];
        Object localObject = (Object) paramArrayOfObject[1];
        Object[] arrayOfObject = (Object[]) paramArrayOfObject[2];
        test(localInteger.intValue(), localObject, arrayOfObject);
        return null;
    }

    private static void test(int age2, Object tempName, Object[] params) {
        System.out.println("out:" + age2 + tempName + params);
    }

    @Override
    public void setHost(Object host) {
        this.host = (App) host;
    }

    public Object getHost() {
        return null;
    }
}