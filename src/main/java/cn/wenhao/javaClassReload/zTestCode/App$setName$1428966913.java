package cn.wenhao.javaClassReload.zTestCode;

import cn.wenhao.javaClassReload.constant.ObjExecutorCache;
import cn.wenhao.javaClassReload.constant.ObjFieldMemberCache;
import cn.wenhao.javaClassReload.invoker.MethodInvoker;
import cn.wenhao.javaClassReload.utils.GenericsUtils;

public class App$setName$1428966913 implements MethodInvoker {
    public App host;

    public Object invoke(Object paramObject, Object... paramArrayOfObject) {
        String str = (String) paramArrayOfObject[0];
        setName(str);
        return null;
    }

    public void setHost(Object host) {
        this.host = ((App) host);
    }

    public void setName(String name) {
        if (GenericsUtils.isNullOrEmpty(name)) {
            return;
        }
        Object localObject1 = Integer.valueOf(12);
        ObjFieldMemberCache.putField("cn/wenhao/javaClassReload/zTestCode/App2", localObject1, "age");
        System.out.println("app2's age is : "
            + ((Integer) ObjFieldMemberCache.getField("cn/wenhao/javaClassReload/zTestCode/App2", "age")).intValue());
        System.out.println("classLoader's name is : " + App2.class.getClassLoader().getClass().getName());
        App2 app2 = new App2();
        localObject1 = name;
        Object localObject2 = app2;
        ObjExecutorCache.invokeMethod(localObject2, "setName(Ljava/lang/String;)V", new Object[] { localObject1 });
        localObject1 = "abdfd";
        localObject2 = Integer.valueOf(23);
        Object localObject3 = name;
        App localApp = this.host;
        boolean isOk =
            ((Boolean) ObjExecutorCache.invokeMethod(localApp, "checkName(Ljava/lang/String;ILjava/lang/String;)Z",
                new Object[] { localObject3, localObject2, localObject1 })).booleanValue();
        System.out.println(" is Ok : " + isOk);
        if (isOk) {
            localObject1 = name;
            localObject2 = this.host;
            ObjFieldMemberCache.putField(localObject2, localObject1, "name");
            if (((Integer) ObjFieldMemberCache.getField(this.host, "age")).intValue() == 123) {
                return;
            }
            name = (String) ObjFieldMemberCache.getField(this.host, "name");
            localObject1 = this.host;
            localObject1 =
                (String) ObjExecutorCache.invokeMethod(localObject1, "getName()Ljava/lang/String;", new Object[0]);
            localObject2 = this.host;
            localObject1 = new Object[] { Integer.valueOf(23), name };
            localObject2 = (Object) ObjExecutorCache.invokeMethod(localObject2,
                "getTempName(Ljava/lang/String;)Ljava/lang/Object;", new Object[] { localObject1 });
            localObject3 = Integer.valueOf(12);
            ObjExecutorCache.invokeMethod("cn/wenhao/javaClassReload/zTestCode/App",
                "test(ILjava/lang/Object;[Ljava/lang/Object;)V",
                new Object[] { localObject3, localObject2, localObject1 });
            System.out.println("invoke over");
        } else {
            throw new IllegalArgumentException("name is illegal");
        }
    }
}