
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.zTestCode;

import cn.wenhao.javaClassReload.invoker.MethodInvoker;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Nov 1, 2016
 * @Desc this guy is too lazy, nothing left.
 */
public class AppChange implements MethodInvoker {

    @Override
    public Object invoke(Object host, Object...argus) {
        return null;
    }

    @Override
    public void setHost(Object host) {

    }

}
