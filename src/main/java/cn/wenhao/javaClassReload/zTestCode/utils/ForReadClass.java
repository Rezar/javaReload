
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.zTestCode.utils;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Nov 1, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public class ForReadClass {
    final int init = 110;
    private final Integer intField = 120;
    public final String stringField = "Public Final Strng Value";
    public static String commStr = "Common String value";
    String str = "Just a string value";
    final double d = 1.1;
    final Double D = 1.2;

    public ForReadClass() {
    }

    public void methodA() {
        System.out.println(intField);
    }

}
