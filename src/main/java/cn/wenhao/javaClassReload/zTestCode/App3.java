
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.zTestCode;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Nov 9, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public class App3 extends App2 {

    public String toString() {
        System.out.println(" i am from app3 ");
        return "app3's toString method ";
    }

}
