
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.zTestCode;

import cn.wenhao.javaClassReload.invoker.MethodInvoker;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Nov 9, 2016
 * @Desc this guy is too lazy, nothing left.
 */
public class App$checkName$613992014_1 implements MethodInvoker {
    public App host;

    public Object invoke(Object paramObject, Object... paramArrayOfObject) {
        String str1 = (String) paramArrayOfObject[0];
        Integer localInteger = (Integer) paramArrayOfObject[1];
        String str2 = (String) paramArrayOfObject[2];
        Boolean localBoolean = Boolean.valueOf(checkName(str1, localInteger.intValue(), str2));
        return localBoolean;
    }

    public void setHost(Object host) {
        this.host = ((App) host);
    }

    public boolean checkName(String name, int temp, String temp2) {
        System.out.println("准备返回 false ");
        return false;
    }
}
