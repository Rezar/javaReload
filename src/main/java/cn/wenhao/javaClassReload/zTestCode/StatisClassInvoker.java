
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.zTestCode;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 28, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public class StatisClassInvoker {

    public static void test(String argus) {
        System.out.println(argus);
    }

    public static void main(String[] args) {
        StatisClassInvoker.test("haha");
    }

}
