
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.zTestCode;

import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;

import cn.wenhao.javaClassReload.utils.GenericsUtils;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 27, 2016
 * @Desc this guy is too lazy, nothing left.
 */
// @Data
public class App extends App2 { // implements FieldMemberExecutor

    /**
     * 
     */

    @Required
    private String name;
    @Required
    private App2 app2 = new App2();

    private int age = 23;

    private App3 app3 = new App3();

    public App(String name) {
    }

    public App() {
        super();
        // System.out.println("haha");
    }

    public void setName(String name) {
        if (GenericsUtils.isNullOrEmpty(name)) {
            return;
        }
        App2.age = 12;
        System.out.println("app2's age is : " + App2.age);
        System.out.println("classLoader's name is : " + App2.class.getClassLoader().getClass().getName());
        App2 app2 = new App2();
        app2.setName(name);
        boolean isOk = this.checkName(name, 23, "abdfd");
        System.out.println(" is Ok : " + isOk);
        if (isOk) {
            this.name = name;
            if (this.age == 123) {
                return;
            }
            name = this.name;
            App.test(12, this.getTempName(this.getName()), 23, name);// this.getTempName(this.getName())
            System.out.println("invoke over");
        } else {
            throw new IllegalArgumentException("name is illegal");
        }
    }

    /**
     * @param age2
     * @param tempName
     * @param i
     * @param name2
     */

    private static void test(int age2, Object tempName, Object...params) {
        System.out.println("in app , --- >out:" + age2 + tempName + params);
    }

    /**
     * @param name2
     * @return
     */

    public Object getTempName(String tempName) {
        return tempName;
    }

    public String getName() {
        return "haha";
    }

    public boolean checkName(String name, int temp, String temp2) {
        return true;
    }

    public String toString() {
         this.setName("i am test ");
        // boolean isNull = this.app2 == null;
        // System.out.println("this.app2 is null ? " + (this.app2 == null));
        // System.out.println("this.app2'classname is : " + this.app2.getClass().getName());
        // System.out.println("this.app3'classname is :  " + this.app3.getClass().getName());
        return "i am test ok";
    }

}
