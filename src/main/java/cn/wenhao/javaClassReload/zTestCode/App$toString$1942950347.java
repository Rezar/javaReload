package cn.wenhao.javaClassReload.zTestCode;

import cn.wenhao.javaClassReload.constant.ObjFieldMemberCache;
import cn.wenhao.javaClassReload.invoker.MethodInvoker;

public class App$toString$1942950347 implements MethodInvoker {
    public App host;

    public Object invoke(Object paramObject, Object...paramArrayOfObject) {
        String str = toString();
        return str;
    }

    public void setHost(Object host) {
        this.host = ((App) host);
    }

    public String toString() {
        boolean isAppNull = ((App2) ObjFieldMemberCache.getField(this.host, "app2") == null);
        // System.out.println("this.app2 is null ? " + ((App2) ObjFieldMemberCache.getField(this.host, "app2") ==
        // null));
        return "i am test ok";
    }
}