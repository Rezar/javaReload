
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.zTestCode;

import cn.wenhao.javaClassReload.JavaClassModify.interfaces.ExecutorAccessor;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 28, 2016
 * @Desc this guy is too lazy, nothing left.
 */
public class App2 {

    public String name;

    public static int age;

    public void setName(String name) {
        this.name = name;
    }

    public App2(String name) {

    }

    public App2() {
        System.out.println("init of app2 " + (this instanceof ExecutorAccessor));
        System.out
            .println("in constructor , classLoader's name is : " + App2.class.getClassLoader().getClass().getName());
    }

    public String toString() {
        return "this is toString method in app2";
    }

}
