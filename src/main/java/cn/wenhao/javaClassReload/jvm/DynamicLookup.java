
 /**
 * Baijiahulian.com Inc.
 * Copyright (c) 2014-2016 All Rights Reserved.
 */
    
package cn.wenhao.javaClassReload.jvm;

import java.lang.reflect.Modifier;
import java.util.List;

/**
 *  @say little Boy, don't be sad.
 *  @name Rezar
 *  @time Nov 10, 2016
 *  @Desc this guy is too lazy, nothing left.
 */
    
public class DynamicLookup {

    private String name;

    private String methodDescriptor;

    /**
     * Create an object capable of performing a dynamic method lookup in some MethodProvider
     * 
     * @param name method name
     * @param methodDescriptor method descriptor (e.g. (Ljava/lang/String;)V)
     */
    public DynamicLookup(String name, String methodDescriptor) {
        this.name = name;
        this.methodDescriptor = methodDescriptor;
    }

    public Invoker lookup(MethodProvider methodProvider) {
        List<Invoker> methods = methodProvider.getDeclaredMethods();
        for (Invoker invoker : methods) {
            if (matches(invoker)) {
                return invoker;
            }
        }
        // Try the superclass context
        MethodProvider parent = methodProvider.getSuper();
        if (parent != null) {
            return lookup(parent);
        }
        return null;
    }

    protected boolean matches(Invoker invoker) {
        return !Modifier.isPrivate(invoker.getModifiers()) && name.equals(invoker.getName())
                && methodDescriptor.equals(invoker.getMethodDescriptor());
    }

}

    