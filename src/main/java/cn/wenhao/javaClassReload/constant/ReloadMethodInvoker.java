
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.constant;

import lombok.Data;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Nov 9, 2016
 * @Desc this guy is too lazy, nothing left.
 */
@Data
public class ReloadMethodInvoker {

    private String newClassName;
    private String methodNameAndDesc;
    private byte[] newMethodInvokerBytes;

    /**
     * @param newClassName
     * @param methodNameAndDesc
     * @param newMethodInvoker
     */
    public ReloadMethodInvoker(String newClassName, String methodNameAndDesc, byte[] newMethodInvokerBytes) {
        super();
        this.newClassName = newClassName;
        this.methodNameAndDesc = methodNameAndDesc;
        this.newMethodInvokerBytes = newMethodInvokerBytes;
    }

}
