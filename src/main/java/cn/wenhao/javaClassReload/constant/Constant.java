
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.constant;

import java.io.Writer;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 28, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public class Constant {

    public static final String DEFAULT_FIELD_FOR_OBJ_EXECUTOR = "_obj_method_executor_$_Rezar_";
    public static final String DEFAULT_FIELD_FOR_OBJ_EXECUTOR_DESC =
        "Lcn/wenhao/javaClassReload/javaClassElements/ObjExecutor;";

    public static final String DEFAULT_FIELD_FOR_OBJ_FIELD_MEMBER = "_obj_field_member_$_Rezar_";
    public static final String DEFAULT_FIELD_FOR_OBJ_FIELD_MEMBER_DESC =
        "Lcn/wenhao/javaClassReload/javaClassElements/ObjFieldMemberExecutor;";

    public static final String DEFAULT_INVOKE_METHOD_NAME = "invokeMethod";

    public static final String DEFAULT_INVOKE_FIELD_PUT_NAME = "putField";
    public static final String DEFAULT_INVOKE_FIELD_PUT_DESC =
        "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V";

    public static final String DEFAULT_INVOKE_FIELD_GET_NAME = "getField";
    public static final String DEFAULT_INVOKE_FIELD_GET_DESC =
        "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;";

    public static final String DEFAULT_INVOKE_METHOD_DESC =
        "(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;";

    // 接口方法定义
    // 方法名称
    public static final String field_member_executor_name = "getObjFieldMemberExecutor";
    // 方法描述
    public static final String field_member_executor_desc =
        "()Lcn/wenhao/javaClassReload/javaClassElements/ObjFieldMemberExecutor;";
    // 方法名称
    public static final String field_method_executor_name = "getObjMethodExecutor";
    // 方法描述
    public static final String field_method_executor_desc =
        "()Lcn/wenhao/javaClassReload/javaClassElements/ObjExecutor;";

    public static final String DEFAULT_GET_OBJ_EXECUTOR_METHOD_NAME = "getObjExecutor";
    public static final String DEFAULT_GET_OBJ_EXECUTOR_METHOD_DESC =
        "(Ljava/lang/Object;)Lcn/wenhao/javaClassReload/javaClassElements/ObjExecutor;";

    public static final String DEFAULT_GET_FIELD_MEMBER_EXECUTOR_METHOD_NAME = "getObjFieldMember";
    public static final String DEFAULT_GET_FIELD_MEMBER_EXECUTOR_METHOD_DESC =
        "(Ljava/lang/Object;)Lcn/wenhao/javaClassReload/javaClassElements/ObjFieldMemberExecutor;";
    
    
    public static final String OUT_PUT_DIRECTORY = "/Users/bjhl/Desktop/bytecodes/";

    public static boolean IS_PRINT = true;
}
