
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Nov 6, 2016
 * @Desc the package need change bytecode
 */
public interface PackageIncludeFilter {

    public boolean needInclude(String className);

    public int order();

}
