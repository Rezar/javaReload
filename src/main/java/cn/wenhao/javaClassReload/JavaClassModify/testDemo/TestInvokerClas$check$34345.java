
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify.testDemo;

import cn.wenhao.javaClassReload.invoker.MethodInvoker;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 28, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public class TestInvokerClas$check$34345 implements MethodInvoker {

    public boolean checkName(String name) {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see cn.wenhao.javaClassReload.invoker.MethodInvoker#invoke(java.lang.Object[])
     */

    @Override
    public Object invoke(Object host, Object...argus) {
        String name = (String) argus[0];
        return ((TestClassInvoker$checkName$23455) host).checkName(name);
    }

    @Override
    public void setHost(Object host) {

    }

}
