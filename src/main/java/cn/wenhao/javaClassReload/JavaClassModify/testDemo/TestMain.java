
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify.testDemo;

import lombok.extern.slf4j.Slf4j;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 28, 2016
 * @Desc this guy is too lazy, nothing left.
 */
@Slf4j
public class TestMain {

    public static void main(String[] args) {
        // MethodInvoker<App> appSetNameInvoker = new TestInvokerClass$setName$2332();
        // MethodInvoker<App> appCheckoutNameInvoker = new TestInvokerClas$check$34345();
        // FieldMember<App> fieldMember = new FieldMember<App>("", 2, "name", "", "");
        // ObjExecutor<App> appObjExecutor = new ObjExecutor<>();
        // appObjExecutor.addFieldInvoker(appObjExecutor.register("name"), fieldMember);
        // appObjExecutor.addMethodInvoker(appObjExecutor.register("setName(Ljava/lang/String;)V"), appSetNameInvoker);
        // appObjExecutor.addMethodInvoker(appObjExecutor.register("checkName(Ljava/lang/String;)B"),
        // appCheckoutNameInvoker);
        //
        // appObjExecutor.hoastClass = App.class.getName();
        // App.executor = appObjExecutor;
        // App app = new App();
        // app.setName("haha");
        // log.info("find app'getName:{}", app.getName());
        //
        // // 进行方法的替换
        // App.executor.addMethodInvoker(appObjExecutor.register("checkName(Ljava/lang/String;)B"),
        // new TestClassInvoker$checkName$23455());
        // app = new App();
        // app.setName("haha");
        // log.info("find app'getName:{}", app.getName());
    }

}
