
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify.testDemo;

import cn.wenhao.javaClassReload.invoker.MethodInvoker;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 28, 2016
 * @Desc this guy is too lazy, nothing left.
 */
public class TestClassInvoker$checkName$23455 implements MethodInvoker {

    public boolean checkName(String name) {
        return false;
    }

    @Override
    public Object invoke(Object host, Object...argus) {
        String name = (String) argus[0];
        return ((TestClassInvoker$checkName$23455) host).checkName(name);
    }

        
    @Override
    public void setHost(Object host) {
        
    }

}
