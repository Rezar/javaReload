
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify.testDemo;

import cn.wenhao.javaClassReload.constant.ObjExecutorCache;
import cn.wenhao.javaClassReload.constant.ObjFieldMemberCache;
import cn.wenhao.javaClassReload.invoker.MethodInvoker;
import cn.wenhao.javaClassReload.javaClassElements.ObjExecutor;
import cn.wenhao.javaClassReload.utils.GenericsUtils;
import cn.wenhao.javaClassReload.zTestCode.App;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 28, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public class TestInvokerClass$setName$2332 implements MethodInvoker {

    public App app;
    private String name;

    public String getHostClass() {
        return "cn/wenhao/javaClassReload/zTestCode/App";
    }

    public boolean hasRet() {
        return true;
    }

    public void setName2(String name) {
        if (GenericsUtils.isNullOrEmpty(name)) {
            return;
        }
        name = (String) ObjFieldMemberCache.getField(this.app, "name");
        this.app.setName(name);
        // App2 app2 = new App2();
        // ObjExecutorCache.invokeMethod(app2, "checkName" + "(Ljava/lang/String;)Z", new Object[] { name });
        boolean isOk = (boolean) ObjExecutorCache.invokeMethod(app, "checkName" + "(Ljava/lang/String;)Z",
            new Object[] { 23, "abdfd", name, name, name, name, name, name });
        if (isOk) {
            ObjFieldMemberCache.putField(app, name, "name");
            if ((int) ObjFieldMemberCache.getField(app, "age") == 123) {
                return;
            }
            String name_ = (String) ObjFieldMemberCache.getField(app, "name");
            ObjExecutorCache.invokeStaticMethod("cn/wenhao/javaClassReload/zTestCode/App",
                "test(ILjava/lang/String;[Ljava/lang/Object;)I",
                (String) ObjExecutorCache.invokeMethod(app, "getName()Ljava/lang/String;"),
                (int) ObjExecutorCache.invokeMethod(app, "getAge()I"));
        } else {
            throw new IllegalArgumentException("name is illegal");
        }
    }

    public void setName(String name) {
        if (GenericsUtils.isNullOrEmpty(name)) {
            return;
        }
        ObjExecutor objExecutor = ObjExecutorCache.getObjExecutor(this.getHostClass());
        Integer key_23434 = objExecutor.register("setName(Ljava/lang/String;)V");
        boolean isOk = (boolean) objExecutor.invokeMethod(key_23434, name);
        if (isOk) {
            ObjFieldMemberCache.putField(this, name, "name");
        } else {
            throw new IllegalArgumentException("name is illegal");
        }
    }

    public Object invoke(Object host, Object...argus) {
        TestInvokerClass$setName$2332 host_ = (TestInvokerClass$setName$2332) host;
        host_.setName((String) argus[0]);
        return null;
    }

    @Override
    public void setHost(Object host) {

    }

}
