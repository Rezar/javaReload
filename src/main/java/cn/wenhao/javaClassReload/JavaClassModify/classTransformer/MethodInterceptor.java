
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify.classTransformer;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Nov 8, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public class MethodInterceptor extends ClassVisitor {

    /**
     * @param api
     * @param cv
     */
    public MethodInterceptor(int api, ClassVisitor cv) {
        super(api, cv);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        if (name.equals("<init>") || name.equals("<clinit>")) {
            return null;
        }
        return super.visitMethod(access, name, desc, signature, exceptions);
    }

}
