
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify.classTransformer;

import org.objectweb.asm.tree.ClassNode;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 28, 2016
 * @Desc this guy is too lazy, nothing left.
 */
public class ClassTransformer {

    protected ClassTransformer ct;

    public ClassTransformer(ClassTransformer ct) {
        this.ct = ct;
    }

    public void transform(ClassNode cn) {
        if (this.ct != null) {
            ct.transform(cn);
        }
    }

}
