
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify.classTransformer.demos;

import java.util.Iterator;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import cn.wenhao.javaClassReload.JavaClassModify.classTransformer.ClassTransformer;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 28, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public class RemoveMethodAdapter extends ClassTransformer {

    private String methodName;
    private String methodDesc;

    /**
     * @param ct
     */
    public RemoveMethodAdapter(ClassTransformer ct, String methodName, String methodDesc) {
        super(ct);
        this.methodName = methodName;
        this.methodDesc = methodDesc;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void transform(ClassNode cn) {
        Iterator<MethodNode> methods = cn.methods.iterator();
        while (methods.hasNext()) {
            MethodNode mn = methods.next();
            if (this.methodName.equals(mn.name) && this.methodDesc.equals(mn.desc)) {
                methods.remove();
            }
        }
        super.transform(cn);
    }

}
