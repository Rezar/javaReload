
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify.classTransformer;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 31, 2016
 * @Desc this guy is too lazy, nothing left.
 */
public class AddMethodClassVisitor extends ClassVisitor implements Opcodes {

    private String name;
    private String desc;
    private MethodNodeVisitor mv;

    /**
     * @param api
     */
    public AddMethodClassVisitor(String name, String desc, MethodNodeVisitor mv) {
        super(ASM4, new ClassWriter(ClassWriter.COMPUTE_MAXS));
        this.name = name;
        this.desc = desc;
        this.mv = mv;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        if (this.name.equals(name) && this.desc.equals(desc)) {
            return this.mv;
        }
        return super.visitMethod(access, name, desc, signature, exceptions);
    }
    
    public ClassWriter getClassWriter(){
        return (ClassWriter) this.cv;
    }

    public MethodNodeVisitor getMethodNodeVisitor() {
        return this.mv;
    }

}
