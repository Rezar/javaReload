
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Nov 7, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public class ClassIncluder {

    private static Set<PackageIncludeFilter> filters =
        new TreeSet<PackageIncludeFilter>(new Comparator<PackageIncludeFilter>() {
            @Override
            public int compare(PackageIncludeFilter o1, PackageIncludeFilter o2) {
                return o2.order() - o1.order();
            }
        });

    public static final List<String> DEFAULT_FILTER_PACKAGES = Arrays.asList("cn.wenhao.javaClassReload.zTestCode");

    static {
        filters.add(new DefaultFilter());
    }

    public static boolean needInclude(String className) {
        boolean needInclude = false;
        for (PackageIncludeFilter includeFilter : filters) {
            needInclude = includeFilter.needInclude(className);
        }
        return needInclude;
    }

    public static void registerPackageIncludeFilter(PackageIncludeFilter packageIncludeFilter) {
        if (packageIncludeFilter != null) {
            filters.add(packageIncludeFilter);
        }
    }

    static class DefaultFilter implements PackageIncludeFilter {

        @Override
        public boolean needInclude(String className) {
            className = className.replace("/", ".");
            boolean needInclude = false;
            for (String needIncludeStr : DEFAULT_FILTER_PACKAGES) {
                needInclude = className.startsWith(needIncludeStr);
            }
            return needInclude;
        }

        @Override
        public int order() {
            return 0;
        }

    }

}
