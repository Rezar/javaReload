
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify.interfaces;

import cn.wenhao.javaClassReload.javaClassElements.ObjExecutor;
import cn.wenhao.javaClassReload.javaClassElements.ObjFieldMemberExecutor;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 31, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public interface ExecutorAccessor {

    /**
     * 获取到当前属性的访问器
     * 
     * @return
     */
    public ObjFieldMemberExecutor getObjFieldMemberExecutor();

    /**
     * 获取当前方法的访问器
     * 
     * @return
     */
    public ObjExecutor getObjMethodExecutor();

}
