
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.JavaClassModify.interfaces;

import cn.wenhao.javaClassReload.javaClassElements.ObjExecutor;
import cn.wenhao.javaClassReload.javaClassElements.ObjFieldMemberExecutor;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Nov 7, 2016
 * @Desc this guy is too lazy, nothing left.
 */

public class AppTest implements ExecutorAccessor {

    public ObjFieldMemberExecutor _obj_member_$_Rezar_;
    public ObjExecutor _obj_method_executor_$_Rezar_;

    @Override
    public ObjFieldMemberExecutor getObjFieldMemberExecutor() {
        return this._obj_member_$_Rezar_;
    }

    @Override
    public ObjExecutor getObjMethodExecutor() {
        return this._obj_method_executor_$_Rezar_;
    }

}
