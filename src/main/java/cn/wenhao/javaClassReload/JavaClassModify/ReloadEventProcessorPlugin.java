
 /**
 * Baijiahulian.com Inc.
 * Copyright (c) 2014-2016 All Rights Reserved.
 */
    
package cn.wenhao.javaClassReload.JavaClassModify;


/**
 *  @say little Boy, don't be sad.
 *  @name Rezar
 *  @time Nov 9, 2016
 *  @Desc this guy is too lazy, nothing left.
 */

public interface ReloadEventProcessorPlugin {
    
    /**
     * Called when a type has been reloaded, allows the plugin to decide if the static initializer should be re-run for
     * the reloaded type. If the reloaded type has a different static initializer, the new one is the one that will run.
     * 
     * @param typename the (dotted) type name, for example java.lang.String
     * @param clazz the Class object that has been reloaded
     * @param encodedTimestamp an encoded time stamp for this version, containing chars (A-Za-z0-9)
     * @return true if the static initializer should be re-run
     */
    boolean shouldRerunStaticInitializer(String typename, Class<?> clazz, String encodedTimestamp);

    // TODO expose detailed delta for changes in the type? (i.e. what new fields/methods/etc)
    // TODO expose instances when they are being tracked?
    /**
     * Called when a type has been reloaded. Note, the class is only truly defined to the VM once, and so the Class
     * object (clazz parameter) is always the same for the same type (ignoring multiple classloader situations). It is
     * passed here so that plugins processing events can clear any cached state related to it. The encodedTimestamp is
     * an encoding of the ID that the agent has assigned to this reloaded version of this type.
     * 
     * @param typename the (dotted) type name, for example java.lang.String
     * @param clazz the Class object that has been reloaded
     * @param encodedTimestamp an encoded time stamp for this version, containing chars (A-Za-z0-9)
     */
    void reloadEvent(String typename, Class<?> clazz, String encodedTimestamp);

}

    