
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.invoker;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 27, 2016
 * @Desc 用于实际调用方法的实现
 */
public interface MethodInvoker {

    /**
     * 为了使用之前的代码,这里的host只传递当前方法的调用器本身对象即可
     * 
     * @param invokeHost
     * @param argus
     * @return
     */
    public Object invoke(Object host, Object...argus);

    public void setHost(Object host);

    // public Object getHost();

}
