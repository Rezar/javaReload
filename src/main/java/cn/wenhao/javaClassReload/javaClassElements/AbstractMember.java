
/**
 * Baijiahulian.com Inc. Copyright (c) 2014-2016 All Rights Reserved.
 */

package cn.wenhao.javaClassReload.javaClassElements;

import java.lang.reflect.Modifier;

import lombok.Data;

/**
 * @say little Boy, don't be sad.
 * @name Rezar
 * @time Oct 27, 2016
 * @Desc 抽象类,用于表示一个属性/方法/构造方法
 */
@Data
public abstract class AbstractMember {

    protected final int modifiers;

    protected final String name;

    protected final String descriptor;

    protected final String signature;

    protected final boolean isPrivate;

    protected final boolean isStatic;

    protected int id = -1;

    protected AbstractMember(int modifiers, String name, String descriptor, String signature) {
        this.modifiers = modifiers;
        this.name = name;
        this.descriptor = descriptor;
        this.signature = signature;
        this.isPrivate = Modifier.isPrivate(this.modifiers);
        this.isStatic = Modifier.isStatic(this.modifiers);
    }

    public int getId() {
        if (this.id == -1) {
            throw new IllegalArgumentException("id not yet allocated");
        } else {
            return this.id;
        }
    }

    public final boolean isStatic() {
        return Modifier.isStatic(getModifiers());
    }

    public final boolean isFinal() {
        return Modifier.isFinal(getModifiers());
    }

    public final boolean isPrivate() {
        return isPrivate;
    }

    public final boolean isProtected() {
        return Modifier.isProtected(getModifiers());
    }

    public final boolean isPublic() {
        return Modifier.isPublic(getModifiers());
    }

}
